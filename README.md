# API-GATEWAY



## Getting started

This is a basic API Gateway developed in Spring Boot 3.2.4 (spring-cloud-starter-gateway 2023.0.0) and Java 21.

It is prepared to connect with two discoveries, Eureka and Consul.

To add, list or refresh the routes of Gateway, use this endpoints below:

```
GET  http://localhost:8181/routes

GET  http://localhost:8181/routes/refresh

POST http://localhost:8181/routes
{
    "routeIdentifier": "ID-in-Gateway-BD,
    "uri": "lb://route-name", //if use this gateway with a discoveries, lb:// its the load balancer protocol and route-name, the app route
    "method": "GET", //GET, POST, PUT, DELETE
    "path": "/app-test/person/**" //Relative path that de user insert after the gateway root URL (http://localhost:8181/app-test/person)
}
```