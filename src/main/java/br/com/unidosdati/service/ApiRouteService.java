package br.com.unidosdati.service;

import org.springframework.stereotype.Service;

import br.com.unidosdati.model.ApiRoute;
import br.com.unidosdati.repository.ApiRouteRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ApiRouteService {
	
	private ApiRouteRepository repository;
	
	public ApiRouteService(ApiRouteRepository repository) {
		this.repository = repository;
	}

    public Flux<ApiRoute> getAll() {
        return this.repository.findAll();
    }

    public Mono<ApiRoute> save(ApiRoute apiRoute) {
        return this.repository.save(apiRoute);
    }
    
    public Mono<Void> delete(ApiRoute apiRoute) {
        return this.repository.delete(apiRoute);
    }

    public Mono<ApiRoute> getById(String id) {
        return this.repository.findById(id);
    }
}
