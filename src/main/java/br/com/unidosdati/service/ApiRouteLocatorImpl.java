package br.com.unidosdati.service;


import java.util.Map;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.BooleanSpec;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.unidosdati.model.ApiRoute;
import reactor.core.publisher.Flux;


@Service
public class ApiRouteLocatorImpl implements RouteLocator {
	
    private final RouteLocatorBuilder routeLocatorBuilder;
    private final ApiRouteService routeService;
    
    public ApiRouteLocatorImpl(RouteLocatorBuilder routeLocatorBuilder, ApiRouteService routeService) {
    	this.routeLocatorBuilder = routeLocatorBuilder;
    	this.routeService = routeService;
    }

    @Override
    public Flux<Route> getRoutes() {
        RouteLocatorBuilder.Builder routesBuilder = routeLocatorBuilder.routes();
        return routeService.getAll()
                .map(apiRoute -> routesBuilder.route(String.valueOf(apiRoute.getRouteIdentifier()),
                        predicateSpec -> setPredicateSpec(apiRoute, predicateSpec)))
                .collectList()
                .flatMapMany(builders -> routesBuilder.build()
                        .getRoutes());
    }

	private Buildable<Route> setPredicateSpec(ApiRoute apiRoute, PredicateSpec predicateSpec) {
		String newPath = getRealPath(apiRoute.getPath(), apiRoute.getUri());
        BooleanSpec booleanSpec = predicateSpec.path(newPath.equals("") ? apiRoute.getPath() : newPath);
        if (!ObjectUtils.isEmpty(apiRoute.getMethod())) {
            booleanSpec.and()
                    .method(apiRoute.getMethod());
        }
        return booleanSpec.uri(apiRoute.getUri());
    }
	
	private String getRealPath(String path, String uri) {
		String prefix = "lb:/";
		String newPath = "";
		int lbIndex = uri.indexOf(prefix);
		if (lbIndex != -1) {
            String prefixToPath = uri.substring(lbIndex + prefix.length());
            newPath = path.replace(prefixToPath, "");
		}
		return newPath;
	}

    @Override
    public Flux<Route> getRoutesByMetadata(Map<String, Object> metadata) {
        return RouteLocator.super.getRoutesByMetadata(metadata);
    }
}  