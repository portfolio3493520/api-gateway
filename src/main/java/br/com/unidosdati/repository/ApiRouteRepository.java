package br.com.unidosdati.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import br.com.unidosdati.model.ApiRoute;

public interface ApiRouteRepository extends ReactiveMongoRepository<ApiRoute,  String> {

}
