package br.com.unidosdati.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.unidosdati.service.ApiRouteLocatorImpl;
import br.com.unidosdati.service.ApiRouteService;

@Configuration
public class RouteLocatorConfig {

    @Bean
    public RouteLocator routeLocator(ApiRouteService routeService, RouteLocatorBuilder routeLocationBuilder) {
        return new ApiRouteLocatorImpl(routeLocationBuilder, routeService);
    }
}
