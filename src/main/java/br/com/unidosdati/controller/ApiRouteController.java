package br.com.unidosdati.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unidosdati.config.GatewayRoutesRefresher;
import br.com.unidosdati.model.ApiRoute;
import br.com.unidosdati.service.ApiRouteService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/routes")
public class ApiRouteController {
    private final ApiRouteService routeService;

    private final GatewayRoutesRefresher gatewayRoutesRefresher;
    
    public ApiRouteController(ApiRouteService routeService, GatewayRoutesRefresher gatewayRoutesRefresher) {
    	this.routeService = routeService;
    	this.gatewayRoutesRefresher = gatewayRoutesRefresher;
    }
    
    @GetMapping
    public ResponseEntity<Flux<ApiRoute>> findAll(){
    	return new ResponseEntity<>(routeService.getAll(), HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    public Mono<ResponseEntity<ApiRoute>> getById(@PathVariable String id) {
        return routeService.getById(id)
        		.map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<ApiRoute>> create(@RequestBody ApiRoute rota) {
    	Mono<ApiRoute> rotaSalva = routeService.save(rota);
    	gatewayRoutesRefresher.refreshRoutes();
        return rotaSalva.map(ResponseEntity::ok);
    }
    
    @PutMapping("/{id}")
    public Mono<ResponseEntity<ApiRoute>> update(@PathVariable String id, @RequestBody ApiRoute updatedRoute) {
        return routeService.getById(id)
                .flatMap(existingRoute -> {
                    existingRoute.setMethod(updatedRoute.getMethod());
                    existingRoute.setPath(updatedRoute.getPath());
                    existingRoute.setRouteIdentifier(updatedRoute.getRouteIdentifier());
                    existingRoute.setUri(updatedRoute.getUri());
                    return routeService.save(existingRoute);
                })
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable String id) {
        return routeService.getById(id)
                .flatMap(existingRoute ->
                        routeService.delete(existingRoute)
                                .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)))
                )
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @GetMapping("/refresh")
    public ResponseEntity<String> refreshRoutes() {
        gatewayRoutesRefresher.refreshRoutes();
        return new ResponseEntity<>("Rotas atualizadas com sucesso", HttpStatus.OK);
    }
}